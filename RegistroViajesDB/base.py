from multiprocessing import connection
import pandas as pd
import os
import sqlite3
import sqlalchemy as db


class Base:
    def __init__(self):

        #Variables de entorno
        self.connect()
    
    def connect(self):   
        try:
            print("intenta conectar")
            self.connection = sqlite3.connect('C:/Users/Lasus/OneDrive - Prevalentware/Documentos/ControlT/RegistroViajesDB/src/DB/test.db', timeout=10)  
            print("Conexión exitosa")
        except Exception as ex:
            print(ex)

    def reconnect(self):
        try:
            self.close()
            self.connect()
        except Exception as ex:
            print(ex)

    def close(self):
        self.connection.close()
        print("desconecta")

    def query(self, query):
        return pd.read_sql_query(query, self.connection)

    def mutate(self, query):
        self.connection.execute(query)
        self.connection.commit()

    def dfToSQL(self, df, table):
        return df.to_sql(name=table, con=self.connection, index = False, if_exists = 'append')

    def querySQLite(self,query):
        cur = self.connection.cursor()
        cur.execute(query)
        self.connection.commit()
        
    # def applyCuid(self,  df):
    #     df["id"] = df.apply(lambda _: cuid.cuid(), axis=1)
    #     return df

