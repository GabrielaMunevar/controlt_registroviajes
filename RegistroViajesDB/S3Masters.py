from base import Base
from datetime import datetime, timedelta
import pandas as pd
import glob
import os


def todayFile(frmt='%d_%m_%Y', string=True):
    todayFile = datetime.now() - timedelta(0)
    if string:
        return todayFile.strftime(frmt)
    return todayFile

class MasterManager(Base):
    def __init__(self):
        Base.__init__(self)
        
    def querys(self,table1,table2,table3):
        ## table2 {table2} TABLE1: TMP_RegistroViajes table3: CFG_CodigosDane
            print("QUERY INSERT INTO")
            self.reconnect()
            print(f"insertando datos de la tabla {table1} a la {table2}...")

            ## QUERY INSERT NUEVOS REGISTROS EN HST 
            #
            #self.querySQLite(f"INSERT INTO {table2}(executeDate, executeNumber, executeNum,fecha,nroRemesa,nroManifiesto,nitCliente,razonCliente,direccionOrigen,ciudadOrigen,direccionDestino,ciudadDestino,codProducto,nomProducto,vrAsegurado,placa,trailer,cedulaConductor,telConductor,firstName,lastName,codeLocationDestinyOrigen,codeLocationDestinyDestino,fechaDestino,codeTypeOperation) SELECT  strftime('%d/%m/%Y %H:%M:%S','now','-6 hours'),0,0,strftime('%d/%m/%Y %H:%M:%S','now','-6 hours'),nroRemesa,nroManifiesto,nitCliente,razonCliente,direccionOrigen,ciudadOrigen,direccionDestino,ciudadDestino,codProducto,nomProducto,vrAsegurado,placa,trailer,cedulaConductor,telConductor,firstName,lastName,codeLocationDestinyOrigen,codeLocationDestinyDestino, strftime('%d/%m/%Y %H:%M:%S','now','-6 hours','+2 days'),CASE WHEN vrAsegurado >= 100000000 THEN 'ALTO'	WHEN vrAsegurado >= 50000000 AND  vrAsegurado <  100000000 THEN 'MEDIO' WHEN vrAsegurado < 50000000 THEN 'BAJO' END FROM TMP_RegistroViajes WHERE NOT EXISTS (SELECT nroManifiesto FROM {table2} WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto)")
            self.querySQLite(f"INSERT INTO {table2}(executeDate, executeNumber, executeNum,fecha,nroRemesa,nroManifiesto,nitCliente,razonCliente,direccionOrigen,ciudadOrigen,direccionDestino,ciudadDestino,codProducto,nomProducto,vrAsegurado,placa,trailer,cedulaConductor,telConductor,firstName,lastName,codeLocationDestinyOrigen,codeLocationDestinyDestino,fechaDestino,codeTypeOperation) SELECT  strftime('%d/%m/%Y %H:%M:%S','now','-6 hours'),0,0,strftime('%d/%m/%Y %H:%M:%S','now','-6 hours'),nroRemesa,nroManifiesto,nitCliente,razonCliente,direccionOrigen,ciudadOrigen,direccionDestino,ciudadDestino,codProducto,nomProducto,vrAsegurado,placa,trailer,cedulaConductor,telConductor,firstName,lastName,codeLocationDestinyOrigen,codeLocationDestinyDestino, strftime('%d/%m/%Y %H:%M:%S','now','-6 hours','+2 days'),CASE WHEN vrAsegurado >= 100000000 THEN 'ALTO'	WHEN vrAsegurado >= 50000000 AND  vrAsegurado <  100000000 THEN 'MEDIO' WHEN vrAsegurado < 50000000 THEN 'BAJO' END FROM {table1} WHERE NOT EXISTS (SELECT nroManifiesto FROM {table2} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto)")

            ## QUERY UPDATE REGISTROS CORREGIDOS
            #"UPDATE {table2} SET razonCliente=(SELECT razonCliente FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),nitCliente      = (SELECT nitCliente FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),direccionOrigen = (SELECT direccionOrigen FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),ciudadOrigen    = (SELECT ciudadOrigen FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),direccionDestino= (SELECT direccionDestino FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),ciudadDestino   = (SELECT ciudadDestino FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),codProducto     = (SELECT codProducto FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),nomProducto     = (SELECT nomProducto FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),vrAsegurado     = (SELECT vrAsegurado FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),placa           = (SELECT placa FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),trailer         = (SELECT trailer FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),cedulaConductor = (SELECT cedulaConductor FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),telConductor   = (SELECT telConductor FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),firstName      = (SELECT firstName FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),lastName       = (SELECT lastName FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),codeLocationDestinyOrigen = (SELECT codeLocationDestinyOrigen FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),codeLocationDestinyDestino = (SELECT codeLocationDestinyDestino FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto),codeTypeOperation = (SELECT CASE WHEN vrAsegurado >= 100000000 THEN 'ALTO' WHEN vrAsegurado >= 50000000 AND  vrAsegurado <  100000000 THEN 'MEDIO' WHEN vrAsegurado < 50000000 THEN 'BAJO' END	FROM TMP_RegistroViajes WHERE {table2}.nroManifiesto = TMP_RegistroViajes.nroManifiesto)WHERE EXISTS (SELECT nroManifiesto FROM {table2})")
            self.querySQLite(f"UPDATE {table2} SET razonCliente= (SELECT razonCliente FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),nitCliente = (SELECT nitCliente FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),direccionOrigen = (SELECT direccionOrigen FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),ciudadOrigen    = (SELECT ciudadOrigen FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),direccionDestino= (SELECT direccionDestino FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),ciudadDestino   = (SELECT ciudadDestino FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),codProducto     = (SELECT codProducto FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),nomProducto     = (SELECT nomProducto FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),vrAsegurado     = (SELECT vrAsegurado FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),placa           = (SELECT placa FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),trailer         = (SELECT trailer FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),cedulaConductor = (SELECT cedulaConductor FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),telConductor   = (SELECT telConductor FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),firstName      = (SELECT firstName FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),lastName       = (SELECT lastName FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),codeLocationDestinyOrigen = (SELECT codeLocationDestinyOrigen FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),codeLocationDestinyDestino = (SELECT codeLocationDestinyDestino FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto),codeTypeOperation = (SELECT CASE WHEN vrAsegurado >= 100000000 THEN 'ALTO' WHEN vrAsegurado >= 50000000 AND  vrAsegurado <  100000000 THEN 'MEDIO' WHEN vrAsegurado < 50000000 THEN 'BAJO' END FROM {table1} WHERE {table2}.nroManifiesto = {table1}.nroManifiesto)WHERE EXISTS (SELECT nroManifiesto FROM {table1} LEFT JOIN {table2} USING(nroManifiesto))")
            #QUERYS ACTUALIZAR DIRECCION Y CIUDAD ORIGEN - DESTINO CODIGOS DANE MAESTRA
            self.querySQLite(f"UPDATE {table2} SET codDaneOrigenMunicipio = {table3}.codDaneMunicipio FROM {table3} WHERE {table2}.ciudadOrigen = {table3}.nomMunicipio")
            self.querySQLite(f"UPDATE {table2} SET codDaneDestinoMunicipio = {table3}.codDaneMunicipio FROM {table3} WHERE {table2}.ciudadDestino = {table3}.nomMunicipio")
            self.querySQLite(f"UPDATE {table2} SET codDaneOrigenDepartamento = {table3}.codDaneDepartamento FROM {table3} WHERE {table2}.ciudadOrigen = {table3}.nomMunicipio")
            self.querySQLite(f"UPDATE {table2} SET codDaneDestinoDepartamento = {table3}.codDaneDepartamento FROM {table3} WHERE {table2}.ciudadDestino = {table3}.nomMunicipio")
            
            # EXECUTE NUMBER 
            self.querySQLite(f"UPDATE {table2} SET executeNumber =(SELECT (IFNULL(max(executeNumber),0) +1) FROM {table2}) WHERE executeNum =0")
            self.querySQLite(f"UPDATE {table2} SET executeNum = 1")
            
            print("inserción finalizada querys")



    def ActualizarDB(self,df,table):
        print("entro a actualizar db")
        self.reconnect()
        print(f"eliminando datos anteriores de la tabla {table}...")
        self.mutate(f"""
                    DELETE from "{table}";
                """)
        print(f"insertando nuevos registros en la tabla {table}...")
        self.dfToSQL(df, table)
        print("inserción finalizada")

    def RegistroViajes(self):
        
        currentDateTime = datetime.now()
        date = currentDateTime.date()
        year = date.strftime("%Y")
        month= date.strftime("%m")
        list_of_files = glob.iglob(f"C:/RPA/RegistroViajes/Docs/Tmp/{year}/{month}/*.xlsx") # * means all if need specific format then *.csv
        latest_file = max(list_of_files, key=os.path.getctime)
        print(latest_file)
 
        df = pd.read_excel(latest_file, engine='openpyxl')
        print(df)
        df = df.rename(columns={
                    'FECHA':"fecha",
                    'NROREMESA':"nroRemesa",
                    'NROMANIFIESTO':"nroManifiesto",
                    'NIT_CLIENTE':"nitCliente",
                    'RAZON_CLIENTE':"razonCliente",
                    'DIRECCION_ORIGEN':"direccionOrigen",
                    'CIUDAD_ORIGEN':"ciudadOrigen",
                    'DIRECCION_DESTINO':"direccionDestino",
                    'CIUDAD_DESTINO':"ciudadDestino",
                    'CODPRODUCTO':"codProducto",
                    'NOMPRODUCTO':"nomProducto",
                    'VRASEGURADO':"vrAsegurado",
                    'PLACA':"placa",
                    'TRAILER':"trailer",
                    'CEDULA_CONDUCTOR':"cedulaConductor",
                    'NOMBRE_CONDUCTOR':"nombreConductor",
                    'TEL_CONDUCTOR':"telConductor"               
        })
        print("1")
        #df = df.drop(['Unnamed: 17'], axis=1)
        ##ELIMINAR CARACTERERES _x000D_ de todo el dataframe
        df = df.replace('_x000D_','', regex=True)
        ##quitar espacios en blanco y recortar los primeros 10 caracteres
        df['telConductor'] = df['telConductor'].str.replace(' ', '').str.slice(0,10)
        ## SEPARAR APELLIDOS Y NOMBRES 
        nombreConductor=df["nombreConductor"].str.split(" ", n = 2, expand = True)
        df = df.drop(['nombreConductor'], axis=1)
        nombreConductor.columns = ['last_name','last_name2','firstName',]
        nombreConductor['lastName'] = nombreConductor.last_name + ' ' + nombreConductor.last_name2
        nombreConductor = nombreConductor.drop(['last_name','last_name2'], axis=1)
        df = pd.concat([df, nombreConductor], axis=1)
        ## CONCATENAR CIUDAD-DIRECCION ORIGEN-DESTINO AGREGAR A DATAFRAME
        df['codeLocationDestinyOrigen']   = df.direccionOrigen + ' ' + df.ciudadOrigen
        df['codeLocationDestinyDestino']  = df.direccionDestino + ' ' + df.ciudadDestino
        
        ## FECHA A DATETIME y campo +48h
        df['fecha']= df['fecha'].str.replace('/','') + '000000'
        df['fecha'] = pd.to_datetime(df['fecha'], format='%d%m%Y%H%M%S')
        df['fechaDestino'] = df["fecha"] + pd.Timedelta('2 days')
        print(df)
        self.ActualizarDB(df, "TMP_RegistroViajes")

    def maestra(self):

        ms = pd.read_excel('C:/RPA/RegistroViajes/Docs/Resources/Codigo_Dane.xlsx',engine='openpyxl')
        ms = ms.drop(['Nombre Departamento','Código Municipio','idCity','lon','lat','idDepartment'],axis=1)
        ms = ms.rename(columns={
                        'Nombre Municipio':"nomMunicipio",
                        'Codigo DANE':"codDaneMunicipio",
                        'Código Deapartamento':"codDaneDepartamento",
                        })
        ms['nomMunicipio'] = ms['nomMunicipio'].str.replace(',', '',regex=True)
        ms['nomMunicipio'] = ms['nomMunicipio'].str.replace('.', '',regex=True)
        ms = ms.dropna(how='all')
        print(ms)
        self.ActualizarDB(ms, "CFG_CodigosDane")
        #self.querys("TMP_RegistroViajes","HST_RegistroViajes","CFG_CodigosDane")
        
        


