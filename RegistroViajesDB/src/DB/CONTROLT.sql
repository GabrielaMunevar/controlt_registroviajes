INSERT OR REPLACE INTO HST_RegistroViajes(fecha,nroRemesa,nroManifiesto,nitCliente,razonCliente,direccionOrigen,ciudadOrigen,direccionDestino,ciudadDestino,codProducto,nomProducto,vrAsegurado,placa,trailer,cedulaConductor,telConductor,firstName,lastName,codeLocationDestinyOrigen,codeLocationDestinyDestino,fechaDestino,codeTypeOperation)
SELECT strftime('%d/%m/%Y %H:%M:%S'),
		nroRemesa,
		nroManifiesto,
		nitCliente,
		razonCliente,
		direccionOrigen,
		ciudadOrigen,
		direccionDestino,
		ciudadDestino,
		codProducto,
		nomProducto,
		vrAsegurado,
		placa,trailer,
		cedulaConductor,
		telConductor,
		firstName,
		lastName,
		codeLocationDestinyOrigen,
		codeLocationDestinyDestino,
		fechaDestino,
		CASE 
			WHEN vrAsegurado >= 100000000 THEN 'ALTO'
			WHEN vrAsegurado >= 50000000 AND  vrAsegurado <  100000000 THEN 'MEDIO'
			WHEN vrAsegurado < 50000000 THEN 'BAJO'
	   END
	   
FROM TMP_RegistroViajes


